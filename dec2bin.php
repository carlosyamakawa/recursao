<p>Faça uma função recursiva que retorne o binário de um número decimal</p>
<p>...php?N=n</p>
<?php
    function dec2bin($N, $i) {
        if ((int)$N/2 == 0) {
            return($N);
        } else {
            return((($N%2) * $i) + dec2bin((int)($N/2), ($i * 10)));
        }
    }

?>
<p><?= "N = ".$_GET['N'] ?></p>
<p>Resultado: <?php echo dec2bin($_GET['N'], 1); ?></p>