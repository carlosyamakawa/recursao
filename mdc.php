<p>Faça uma função recursiva que calcule o MDC de dois números X e Y</p>
<p>...php?X=x&Y=y</p>
<?php
    function mdc($X, $Y) {
        if ($X % $Y == 0) {
            return($Y);
        } else {
            return(mdc($Y, $X%$Y));
        }
    }

?>
<p><?= "X = ".$_GET['X']." | Y = ".$_GET['Y'] ?></p>
<p>Resultado: <?php echo mdc($_GET['X'], $_GET['Y']); ?></p>