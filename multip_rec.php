<p>11) A multiplicação de dois números inteiros pode ser feita através de somas sucessivas. Proponha um algoritmo recursivo Multip_Rec(n1,n2) que calcule a multiplicação de dois inteiros.</p>
<p>...php?n1=x&n2=y</p>
<?php
    function multip_rec($n1, $n2) {
        if ($n1 == 0 || $n2 == 0) {
            return(0);
        } else if ($n2 == 1) {
            return($n1);
        } else {
            return($n1 + multip_rec($n1, $n2-1));
        }
    }

?>
<p><?= "N1 = ".$_GET['n1']."<br>N2=".$_GET['n2'] ?></p>
<p>Resultado: <?php echo multip_rec($_GET['n1'], $_GET['n2']); ?></p>