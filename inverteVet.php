<p>Faça um programa que recebe um vetor que o inverta</p>
<p>...php?vetor=x,y,z,w,...</p>
<?php
    function inverteVet(&$v, $ini, $fim) {
        if ($ini < $fim) {
            $aux = $v[$ini];
            $v[$ini] = $v[$fim];
            $v[$fim] = $aux;
            inverteVet($v, $ini+1, $fim-1);
        }
    }

?>
<p><?= "Vetor original: ".$_GET['vetor'] ?></p>
<?php

    $v = explode(',', $_GET['vetor']); //add comentário

    inverteVet($v, 0, count($v)-1);
?>
<p>Resultado: <?php for ($i=0;$i<10;$i++) { echo $v[$i].','; } ?></p>