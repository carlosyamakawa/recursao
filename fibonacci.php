<p>Faça uma função recursiva que calcule e retorne o N-ésimo termo da sequência Fibonacci. Alguns números desta sequência são: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89...</p>
<p>...php?N=x</p>
<?php
    function fibonacci($N) {
        if ($N == 1) {
            return(0);
        } else if ($N <= 3) {
            return(1);
        } else {
            return(fibonacci($N-1) + fibonacci($N-2));
        }
    }

?>
<p><?= "N = ".$_GET['N'] ?></p>
<p>Resultado: <?php echo fibonacci($_GET['N']); ?></p>