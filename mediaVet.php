<p>46) Faça uma função recursiva que permita calcular a média de um vetor de tamanho N</p>
<p>...php?vetor=x,y,z,w,...</p>
<?php
    function mediaVet($v, $i, $N) {
        if ($i == 0) {
            return($v[$i]/$N);
        } else {
            return($v[$i]/$N + mediaVet($v, $i-1, $N));
        }
    }

    $v = explode(',', $_GET['vetor']); //add comentário
?>
<p>Resultado: <?= mediaVet($v, count($v)-1, count($v)) ?></p>