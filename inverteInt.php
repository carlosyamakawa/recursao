<p>3) Faça uma função recursiva que permita inverter um número inteiro N. Ex: 123 -> 321</p>
<p>...php?n=x</p>
<?php
    function qtdDigito($n) {
        if ($n < 10) {
            return(1);
        } else {
            return(10 * qtdDigito((int)($n/10)));
        }
    }

    function inverteInt($n) {
        if ($n < 10) {
            return($n);
        } else {
            return( (($n%10) * qtdDigito($n)) + inverteInt((int)($n/10)) );
        }
    }

?>
<p><?= "N = ".$_GET['n'] ?></p>
<p>Resultado: <?php echo inverteInt($_GET['n']); ?></p>